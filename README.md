# Engineering Play Book

## Introduction

The following are rules guiding our coding process. Common linting and prettier standards are listed below, 
and they are based on our work with TypeScript, which is our development language.
Projects should extend these linting and prettier standards within their codebase as may be needed.